
## hidraw
device = "/dev/hidraw0"
package_length = 5

with open(device, "rb") as file:
	last = ""
	while True:
		next = file.read(package_length)
		if next != last:
			token = list(next)
			for n in range(2):
				token[2+n] = (token[2+n]%128)
			token[4] = token[4]-95
			print (token)
		last = next	
