# !/usr/bin/python

# Copyright (c) 2018 Raphael Groner
# License: MIT
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys
import re
import secrets
import requests
import os

url_server = 'http://192.168.123.96/ConvertService.ashx'
outputtype = 'pdf'
#viewer = 'firefox -new-window -private-window'
#viewer = 'wget -O converted-{key}.pdf'
viewer = 'evince'

def checkSourceURL(params):
    if len(params) >1:
        url_source = params[1]
    else:
        print('Fehler: Bitte URL angeben.')
        sys.exit(1)
    return url_source

def getFileType(url_source):
    m = re.compile('.*\.(.*)').match(url_source)
    if m:
        filetype = m.group(1)
    else:
        filetype = ''
    return filetype

def upload(url_source):
    #filetype = 'docx'
    filetype = getFileType(url_source)
    print('Type: "{}"'.format(filetype))
    token = secrets.token_urlsafe()
    print('Token: "{}"'.format(token))
    
    print('Upload ...')
    data = { 'filetype':filetype, 'key':token, 'outputtype':outputtype, 'url':url_source }
    r = requests.post(url=url_server, json=data)
    if r.status_code == 200:
        p = re.compile('.*<FileUrl>(.*conv_{key}_{outputtype}.*)</FileUrl>'.format(**data))
        m = p.match(r.text)
        if m:
            print('OK')
            target = m.group(1).replace('&amp;', '&')
            command = '{} "{}"'.format(viewer.format(**data), target)
            print(command)
            os.system(command)
        else:
            print('Fehler: URL nicht erhalten oder nicht gefunden!')
            print(r.text)
    else:
        print('Fehler: {}'.format(r.status_code))
        print(r.text)

if __name__ == "__main__":
    #url_source = 'http://www.kalenderpedia.de/download/stundenplan/stundenplan-montag-bis-freitag-in-farbe.docx'
    url_source = checkSourceURL(sys.argv)
    upload(url_source)
