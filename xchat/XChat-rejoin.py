#!/usr/bin/python
import xchat

def notice_check(word, word_eol, userdata):
    if word_eol[3].startswith(':NickServ set your hostname to'):
        joinchannels()
    return xchat.EAT_NONE

def joinchannels(*args):
    global JOIN_CHANNELS
    for chan in JOIN_CHANNELS.split(','):
        chan = chan.strip()
        if not chan.startswith('#'):
            chan = '#' + chan
        xchat.command('join %s' % chan)


xchat.hook_server('NOTICE', notice_check)
xchat.command('msg nickserv identify %s' % PASSWORD)
