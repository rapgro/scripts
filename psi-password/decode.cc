/*
 * http://www.georglutz.de/blog/2005/07/01/recover-lost-jabber-passwords-in-psis-config-files/
 * Compile with:
 * $ g++ -L$QTDIR/lib decode.cc -lqt-mt
 * Achtung! Braucht Qt3 header files, Fedora: qt3-devel
 */

#include <qstring.h>
#include <iostream>

int hexChar2int(char c)
{
        if(c >= 'A' && c <= 'F')
                return c - 'A' + 10;
        else if(c >= 'a' && c <= 'f')
                return c - 'a' + 10;
        else if(c >= '0' && c <= '9')
                return c - '0';

        return 0;
}

QString decodePassword(const QString &pass, const QString &key)
{
        QString result;
        unsigned int n1, n2;

        if(key.length() == 0)
                return pass;

        for(n1 = 0, n2 = 0; n1 < pass.length(); n1 += 4) {
                ushort x = 0;
                if(n1 + 4 > pass.length())
                        break;
                x += hexChar2int(pass.at(n1))*4096;
                x += hexChar2int(pass.at(n1+1))*256;
                x += hexChar2int(pass.at(n1+2))*16;
                x += hexChar2int(pass.at(n1+3));
                QChar c(x ^ key.at(n2++).unicode());
                result += c;
                if(n2 >= key.length())
                        n2 = 0;
        }
        return result;
}

int main()
{
    QString password = "aaa000000000000000000000000000000000000000000";
    QString key = "id@jabber.org";
    QString out = decodePassword(password,key);
    std::cout << out << std::endl;
}

