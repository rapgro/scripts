#!/bin/bash
echo Checking md5sum of $1
MD5=$(( $(ls -l $1 | awk '{print  $5}' ) ))
echo Count is $MD5
cat $1 | md5sum
dd if=/dev/sr0 bs=1 count=$MD5 | md5sum
